<?php
	include '../init.php';
	$user_id = $_SESSION['user_id'];
	$user = $users->user_data($user_id);

	$user_type = $user['type'];

	$post_id = $_GET['post_id'];
	$post_details = $posts->get_post($post_id);
	$user_type = $user['type'];
	$success = 0;
	if(isset($_POST['edit'])){
		$edit_title = $_POST['title'];
		$edit_body = $_POST['body'];
		$title = 'title';
		$body = 'body';

		if( ! empty($edit_title) || ! empty($edit_body)){
			if($edit_title != null){
				$posts->edit_post($post_id,$title,$edit_title);
			}
			if($edit_body != Null){
				$posts->edit_post($post_id,$body,$edit_body);
			}
			header("Refresh:0");
		}
		else{
			$errors[] = "Fields are empty";
		}
	}



?>
<!DOCTYPE html>
<html lang="en">
	<?php include 'header.php'; ?>

	<body>
		<?php include 'navbar.php'; ?>

		<div class="container" style="margin-top:100px;">
			<div class="row">
				<div class="col-md-8">
					<h2><a href="#"> <?php echo $post_details['title']; ?> </a></h2>
					<p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_details['created_at']; ?></p>
					<hr></hr>
					<p><?php echo $post_details['body']; ?></p>
					<a class="btn btn-primary" href="#openModal"><span class="glyphicon glyphicon-edit"> </span> Edit </a>
					<hr></h>
				</div>
			 </div>
		</div>
		<script>
			$(function(){
				document.location.href = '#openModal';
			});
		</script>

		<div id="openModal" class="modalDialog2">
			<div>
				<a href="#close" title="Close" class="close"><i class="glyphicon glyphicon-remove"></i></a>
				<div class="row">
					<form class="form-signup" action="" method="post">
						<div class="well well-sm"><strong><span class="glyphicon glyphicon-edit"></span>  Edit Post</strong></div>
						<div class="form-group">
							<label for="InputName"> Title</label>
							<div class="input-group">
								<input type="text" class="form-control" name="title" id="edit_title" placeholder="Enter Title" value="<?php echo $post_details['title'];?>">
								<span class="input-group-addon"></span>
							</div>
						</div>
						<div class="form-group">
							<label for="InputName"> Body</label>
							<div class="input-group">
								<textarea type="text" class="form-control" name="body" id="edit_body" placeholder="Enter Body" style="height:200px" ><?php echo $post_details['body'];?></textarea>
								<span class="input-group-addon"></span>
							</div>
						</div>
						<input type="submit" name="edit" id="edit" value="Edit" class="btn btn-primary pull-right">
					</form>
					<div class="col-lg-5 col-md-push-1">
						<div class="col-md-12">
							<?php if(!empty($errors)){ ?>
								<div class="alert alert-danger">
									<span class="glyphicon glyphicon-remove"></span><strong><?php echo '<p>*' . implode('</p><p>*', $errors) . '</p>'; ?></strong>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include 'footer.php'; ?>
	</body>
</html>

