<?php
	include '../init.php';

	$user_id = $_SESSION['user_id'];
	$user = $users->user_data($user_id);

	$user_type = $user['type'];

	if(isset($_POST['edit'])){
		$users->edit_user();
		header("Refresh:0");
	}
?>
<!DOCTYPE html>
<html lang="en">
	<?php include 'header.php'; ?>

	<body>
		<?php include 'navbar.php'; ?>

		<div class="container" style="margin-top:80px;">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo $user['first_name'].' '.$user['middle_name'].' '.$user['last_name'];?></h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3 col-lg- " style="top:20px;"align="center"> <img alt="User Pic" src="css/index.png" class="img-circle img-responsive"> </div>
								<div class=" col-md-9 col-lg-9 ">
									<table class="table table-user-information">
										<tbody>
											<tr>
												<td>Email Address:</td>
												<td><?php echo $user['email_address'];?></td>
											</tr>
											<tr>
												<td>Address</td>
												<td><?php echo $user['address'];?></td>
											</tr>
											<tr>
												<td>Contact Number</td>
												<td><?php echo $user['contact_number'];?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="panel-footer">
							<span class="pull-right">
								<p><a class="btn btn-primary" href="#openModal" role="button"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>  Edit Profile </button></a></p>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			$(function() {
				document.location.href = '#openModal';
			});
		</script>

		<div id="openModal" class="modalDialog2">
			<div>
				<a href="#close" title="Close" class="close"><i class="glyphicon glyphicon-remove"></i></a>
				<div class="row">
					<form class="form-signup" action="" method="post">
						<div class="col-lg-10">
							<div class="well well-sm"><strong><span class="glyphicon glyphicon-edit"></span> Edit Profile</strong></div>
							<div class="form-group">
								<label for="InputName"> First Name</label>
								<div class="input-group">
									<input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter First Name" value="<?php echo $user['first_name'];?>">
									<span class="input-group-addon"><span class="glyphicon glyphicon-edit"></span></span>
								</div>
							</div>
							<div class="form-group">
								<label for="InputName"> Middle Name</label>
								<div class="input-group">
									<input type="text" class="form-control" name="middle_name" id="middle_name" placeholder="Enter Middle Name" value="<?php echo $user['middle_name'];?>">
									<span class="input-group-addon"><span class="glyphicon glyphicon-edit"></span></span>
								</div>
							</div>
							<div class="form-group">
								<label for="InputName"> Last Name</label>
								<div class="input-group">
									<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name" value="<?php echo $user['last_name'];?>">
									<span class="input-group-addon"><span class="glyphicon glyphicon-edit"></span></span>
								</div>
							</div>
							<div class="form-group">
								<label for="InputName"> Address </label>
								<div class="input-group">
									<input type="text" class="form-control" name="address" id="address" placeholder="Enter Address" value="<?php echo $user['address'];?>">
									<span class="input-group-addon"><span class="glyphicon glyphicon-edit"></span></span>
								</div>
							</div>
							<div class="form-group">
								<label for="InputName"> Contact Number</label>
								<div class="input-group">
									<input type="text" class="form-control" name="contact_number" id="contact_number" placeholder="Enter Contact Number" value="<?php echo $user['contact_number'];?>">
									<span class="input-group-addon"><span class="glyphicon glyphicon-edit"></span></span>
								</div>
							</div>
							<div class="form-group">
								<label for="InputEmail"> Email</label>
								<div class="input-group">
									<input type="email" class="form-control" id="email_address" name="email_address" placeholder="Enter Email" value="<?php echo $user['email_address'];?>">
									<span class="input-group-addon"><span class="glyphicon glyphicon-edit"></span></span>
								</div>
							</div>
							<div class="form-group">
								<label for="InputEmail"> Password</label>
								<div class="input-group">
									<input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" >
									<span class="input-group-addon"><span class="glyphicon glyphicon-edit"></span></span>
								</div>
							</div>
							<div class="form-group">
								<label for="InputEmail">Confirm Password</label>
								<div class="input-group">
									<input type="password" class="form-control" id="confpass" name="confpass" placeholder="Confirm Password" >
									<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
								</div>
							</div>
							<input type="submit" name="edit" id="edit" value="Edit" class="btn btn-primary pull-right">
						</div>
					</form>
					<div class="col-lg-5 col-md-push-1">
						<div class="col-md-12">

							<?php if(!empty($errors)){ ?>
								<div class="alert alert-danger">
									<span class="glyphicon glyphicon-remove"></span><strong><?php echo '<p>*' . implode('</p><p>*', $errors) . '</p>'; ?></strong>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include 'footer.php'; ?>
	</body>
</html>

