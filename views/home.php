<?php
	include '../init.php';
	$user_id = $_SESSION['user_id'];
	$user = $users->user_data($user_id);

	$user_type = $user['type'];
	$all_posts = $posts->get_all_posts();
	$post = 0;

	if(isset($_POST['add'])){
		$post = $posts->add_post();
		if($post == true){
			header("Refresh:0");
		}
	}

	if(isset($_POST['delete'])){
		$post_id = $_POST['post_id'];
		$posts->delete_post($post_id);
		header("Refresh:0");
	}
?>
<!DOCTYPE html>
<html lang="en">
	<?php include 'header.php'; ?>

	<body>
		<?php include 'navbar.php'; ?>

		<div class="container" style="margin-top:100px;">
			<div class="row">
				<div class="col-md-8">
					<?php foreach($all_posts as $mp){ ?>
						<h2 style="color:teal;"> <?php echo $mp['title']; ?> </h2>
						<p class="lead" style="color:teal;">by <?php $name = $users->user_data($mp['user_id']); echo $name['first_name'].' '.$name['middle_name'].' '.$name['last_name'];?></p>
						<p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $mp['created_at']; ?></p>
						<hr></hr>
						<p><?php echo substr($mp['body'], 0, 250); ?></p>
						<?php if($user_type == 'Admin' or $mp['user_id'] == $user_id){ ?>
						<?php echo "<a class='btn btn-primary' href=view_post.php?post_id=",$mp['id'],"><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>Edit</a>"; ?>
						<?php echo '<form action="" method="post"><input type="hidden" name="post_id" id="post_id" value="'.$mp['id'].'" /><input type="submit" class="btn btn-danger" name="delete" id="delete" value="Delete"></form>'; ?>
						<?php } ?>
						<hr></hr>
					<?php } ?>


				</div>
				<div class="col-md-4">
					<div class="well">
						<p><a class="btn btn-lg loginbut" href="#openModal" role="button"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>  Add Entry </button></a></p>
					</div>

				</div>
			</div>
		</div>
		<script>
			$(function(){
				document.location.href = '#openModal';
			});
		</script>

		<div id="openModal" class="modalDialog2">
			<div>
				<a href="#close" title="Close" class="close"><i class="glyphicon glyphicon-remove"></i></a>
				<div class="row">
					<form class="form-signup" action="" method="post">
						<div class="well well-sm"><strong><span class="glyphicon glyphicon-pencil"></span>  New Entry</strong></div>
						<div class="form-group">
							<label for="InputName"> Title</label>
							<div class="input-group">
								<input type="text" class="form-control" name="title" id="title" placeholder="Enter Title" >
								<span class="input-group-addon"></span>
							</div>
						</div>
						<div class="form-group">
							<label for="InputName"> Body</label>
							<div class="input-group">
								<textarea class="form-control" name="body" id="body" placeholder="Enter Body" style="height:200px"></textarea>
								<span class="input-group-addon"></span>
							</div>
						</div>
						<input type="submit" name="add" id="add" value="Add" class="btn btn-primary pull-right">
					</form>
					<div class="col-lg-5 col-md-push-1">
						<div class="col-md-12">
							<?php if($post === false){ ?>
								<div class="alert alert-danger">
									<span class="glyphicon glyphicon-remove"></span><strong>Fields are empty!</strong>
								</div>
							<?php }?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>

