<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button class="navbar-toggle" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" ><?php echo 'Welcome '.$user['first_name'].'!';?></a>
		</div>
		<div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="home.php">Home</a></li>
				<li><a href="profile.php">Profile</a></li>
				<?php if($user_type == 'Admin'){ ?>
				<li><a href="admin_users.php">Users</a></li>
				<?php } ?>
				<li><a href="logout.php">Logout</a></li>
			</ul>
		</div>
	</div>
</nav>