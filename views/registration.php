<?php
	$success = 0;
	/*echo "<pre>"; print_r($_POST); echo "</pre>";*/
	if(isset($_POST['register'])) {
		include '../init.php';
		$result = $users->register();
		if($result === true) {
			header('location:index.php');
		}
		else
		{
			$errors = $result;
		}
	}
?>
<!DOCTYPE html>
	<head>
		<link rel="stylesheet" href="../css/firstproj.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	</head>

	<body>
		<div class="container" style="margin-top:50px;">
			<div class="row">
				<form class="form-signup" name="form_reg" method="post">
					<div class="col-lg-6">
						<div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required Field</strong></div>
						<div class="form-group">
							<label for="InputName"> First Name</label>
							<div class="input-group">
								<input type="text" class="form-control" name="first_name" id="fname" placeholder="Enter First Name" value="<?php echo ( ! empty($_POST['first_name'])) ? $_POST['first_name'] : '';?>" >
								<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
							</div>
						</div>
						<div class="form-group">
							<label for="InputName"> Middle Name</label>
							<div class="input-group">
								<input type="text" class="form-control" name="middle_name" id="mname" placeholder="Enter Middle Name" >
								<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
							</div>
							<span class="text-danger"><?php echo ( ! empty($errors['middle_name_error'])) ? $errors['middle_name_error'] : ''?></span>
						</div>
						<div class="form-group">
							<label for="InputName"> Last Name</label>
							<div class="input-group">
								<input type="text" class="form-control" name="last_name" id="lname" placeholder="Enter Last Name" >
								<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
							</div>
						</div>
						<div class="form-group">
							<label for="InputName"> Address </label>
							<div class="input-group">
								<input type="text" class="form-control" name="address" id="address" placeholder="Enter Address" >
								<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
							</div>
						</div>
						<div class="form-group">
							<label for="InputName"> Contact Number</label>
							<div class="input-group">
								<input type="text" class="form-control" name="contact_number" id="cnum" placeholder="Enter Contact Number" >
								<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
							</div>
						</div>
						<div class="form-group">
							<label for="InputEmail"> Email</label>
							<div class="input-group">
								<input type="email" class="form-control" id="email_address" name="email_address" placeholder="Enter Email" >
								<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
							</div>
							<span class="text-danger"><?php echo ( ! empty($errors['email_address_error'])) ? $errors['email_address_error'] : ''?></span>
						</div>
						<div class="form-group">
							<label for="InputEmail"> Password</label>
							<div class="input-group">
								<input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" >
								<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
							</div>
						</div>
						<div class="form-group">
							<label for="InputEmail">Confirm Password</label>
							<div class="input-group">
								<input type="password" class="form-control" id="confpass" name="confpass" placeholder="Confirm Password" >
								<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
							</div>
						</div>
						<div class="form-group">
							<label for="InputEmail">User Type</label>
							<select name="type" class="form-control">
								<option>Author</option>
								<option>Admin</option>
							</select>
						</div>
						<input type="submit" name="register" id="register" value="Register" class="btn btn-info pull-right">
					</div>
				</form>
				<div class="col-lg-5 col-md-push-1">
					<div class="col-md-12">
						<?php if($status == 1){ ?>
							<div class="alert alert-success">
								<strong><span class="glyphicon glyphicon-ok"></span>You are now registered!<a href="index.php">Please click to login</a></strong>
							</div>
						<?php } ?>
						<?php if(!empty($error)){ ?>
							<div class="alert alert-danger">
								<span class="glyphicon glyphicon-remove"></span><strong><?php echo $error ?></strong>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>