<?php
	if(empty($_POST) === false){

		$email = $_POST['email'];
		$password = $_POST['password'];
		include '../init.php';

		$login = $users->login($email, $password);

		if($login === false) {
			$errors[] = 'Sorry username/password is invalid';
		}
		else{
			$_SESSION['user_id'] = $login;
			$user = $users->user_data($_SESSION['user_id']);
			header('Location: home.php');
			exit();
		}
	}
?>
<!DOCTYPE html>
	<?php include 'header.php' ?>
	<body>
		<div class="container">
			<form class="form-signin" method="post">
				<h2 class="form-signin-heading">Please Sign in</h2>
				<input class="form-control" id="email" placeholder="Email Address" name="email">
				<input type ="password" class="form-control" id="password" placeholder="Password" name="password">
				<div class="nreg">
					<label for="InputName">
						<a href="registration.php">Register as new user</a>
					</label>
				</div>
				<button class="btn btn-lg btn-primary btn-block" type="submit" name="login">Log in</button>
				<?php if(!empty($errors)){ ?>
					<div class="alert alert-danger" style="margin-top:10px;">
					<strong><?php echo '<p>*' . implode('</p><p>*', $errors) . '</p>';?></strong>
					</div>
				<?php } ?>
			</form>
		</div>
	</body>
</html>