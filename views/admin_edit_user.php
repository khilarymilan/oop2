<?php
	include '../init.php';

	$user_id = $_SESSION['user_id'];
	$user = $users->user_data($user_id);
	$user_type = $user['type'];
	$edit_user_id = $_GET['edit_user_id'];
	$user_data = $users->user_data($edit_user_id);

	if(isset($_POST['edit'])){
		$edit_fname = $_POST['edit_fname'];
		$edit_mname = $_POST['edit_mname'];
		$edit_lname = $_POST['edit_lname'];
		$edit_address = $_POST['edit_address'];
		$edit_cnum = $_POST['edit_cnum'];

		$users->edit_user();

	}
?>
<!DOCTYPE html>
<html lang="en">
	<?php include 'header.php'; ?>

	<body>
		<?php include 'navbar.php'; ?>

		<div class="container" style="margin-top:80px;">
			<div class="row">
				<form class="form-signup" action="" method="post">
					<div class="well well-sm"><strong><span class="glyphicon glyphicon-edit"></span>  Edit User</strong></div>
					<div class="form-group">
						<label for="InputName"> First Name</label>
						<div class="input-group">
							<input type="text" class="form-control" name="first_name" id="edit_fname" placeholder="First Name" value="<?php echo $user_data['first_name'];?>">
							<span class="input-group-addon"></span>
						</div>
					</div>
					<div class="form-group">
						<label for="InputName"> Middle Name</label>
						<div class="input-group">
							<input type="text" class="form-control" name="last_name" id="edit_mname" placeholder="Middle Name" value="<?php echo $user_data['middle_name'];?>">
							<span class="input-group-addon"></span>
						</div>
					</div>
					<div class="form-group">
						<label for="InputName"> Last Name</label>
						<div class="input-group">
							<input type="text" class="form-control" name="middle_name" id="edit_lname" placeholder="Last Name" value="<?php echo $user_data['last_name'];?>">
							<span class="input-group-addon"></span>
						</div>
					</div>
					<div class="form-group">
						<label for="InputName"> Address</label>
						<div class="input-group">
							<input type="text" class="form-control" name="address" id="edit_address" placeholder="Address" value="<?php echo $user_data['address'];?>">
							<span class="input-group-addon"></span>
						</div>
					</div>
					<div class="form-group">
						<label for="InputName"> Contact Number</label>
						<div class="input-group">
							<input type="text" class="form-control" name="contact_number" id="edit_cnum" placeholder="Contact Number " value="<?php echo $user_data['contact_number'];?>">
							<span class="input-group-addon"></span>
						</div>
					</div>
					<input type="submit" name="edit" id="edit" value="Edit" class="btn btn-primary pull-right">
				</form>
				<div class="col-lg-5 col-md-push-1">
					<div class="col-md-12">
						<?php if(!empty($errors)){ ?>
							<div class="alert alert-danger">
								<span class="glyphicon glyphicon-remove"></span><strong><?php echo '<p>*' . implode('</p><p>*', $errors) . '</p>'; ?></strong>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php include 'footer.php'; ?>
	</body>
</html>

