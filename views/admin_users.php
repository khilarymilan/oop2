<?php
	include '../init.php';
	$user_id = $_SESSION['user_id'];
	$user = $users->user_data($user_id);

	$user_type = $user['type'];
	$all_users = $users->get_users();

	if(isset($_POST['delete'])){
		$edit_id = $_POST['edit_id'];
		$users->delete_user($edit_id);
		header("Refresh:0");
	}

?>
<!DOCTYPE html>
<html lang="en">
	<?php include 'header.php'; ?>

	<body>
		<?php include 'navbar.php'; ?>
		<div class="container" style="margin-top:80px;">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
				<?php foreach($all_users as $details){ ?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo $details['first_name'].' '.$details['middle_name'].' '.$details['last_name'];?></h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3 col-lg- " style="top:20px;"align="center"> <img alt="User Pic" src="css/index.png" class="img-circle img-responsive"> </div>
								<div class=" col-md-9 col-lg-9 ">
									<table class="table table-user-information">
										<tbody>
											<tr>
												<td>Email Address:</td>
												<td><?php echo $details['email_address'];?></td>
											</tr>
											<tr>
												<td>Address</td>
												<td><?php echo $details['address'];?></td>
											</tr>
											<tr>
												<td>Contact Number</td>
												<td><?php echo $details['contact_number'];?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<span class="pull-right">
								<?php echo "<a class='btn btn-primary' href=admin_edit_user.php?edit_user_id=",$details['id'],"><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>Edit</a>"; ?>
								<?php echo '<form action="" method="post"><input type="hidden" name="edit_id" id="edit_id" value="'.$details['id'].'" /><input type="submit" class="btn btn-sm btn-danger" name="delete" id="delete" value="Delete"></form>'; ?>
							</span>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>

		<?php include 'footer.php'; ?>
	</body>
</html>

