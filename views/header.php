	<head>
		<meta charset="utf-8"></meta>
		<meta content="IE=edge" http-equiv="X-UA-Compatible"></meta>
		<meta content="width=device-width, initial-scale=1" name="viewport"></meta>
		<meta content="" name="description"></meta>
		<meta content="" name="author"></meta>
		<link rel="stylesheet" href="../css/firstproj.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="css/bootstrap.min.css"></link>
		<style>
			.modalDialog2{
				position: fixed;
				top: 0;
				right: 0;
				bottom: 0;
				left: 0;
				background: rgba(0,0,0,0.8);
				z-index: 99999;
				opacity:0;
				-webkit-transition: opacity 400ms ease-in;
				-moz-transition: opacity 400ms ease-in;
				transition: opacity 400ms ease-in;
				pointer-events: none;
			}
			.modalDialog2:target{
				opacity:1;
				pointer-events: auto;
			}

			.modalDialog2 > div{
				width:520px;
				position: relative;
				margin-left:auto;
				margin-right:auto;
				margin-top:20px;
				margin-bottom:20px;
				padding: 30px;
				border-radius: 5px;
				background: #fff;
				text-align: center;
			}
			.close{
				color: #13404A;
				line-height: 25px;
				position: absolute;
				right: 2px;
				text-align: center;
				top: 2px;
				width: 24px;
			}
		</style>
		<title>First Task</title>
	</head>