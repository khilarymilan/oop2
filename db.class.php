<?php

class Database{

	private $db;
	public function __construct(){

	}

	public function connect(){
		$config = array(
			'host' => 'localhost',
			'username' => 'root',
			'password' => '123',
			'dbname' => 'oop'
		);
		return $this->db = new PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'], $config['username'], $config['password']);
	}

	public function set_date(){

		date_default_timezone_get('Asia/Manila');
		return $todate = date('Y-m-d h:i:s');
	}

	public function insert($table, $fields = array()) {
		$this->connect();
		$keys = array_keys($fields);
		$values = null;
		foreach(array_values($fields) as $value) {
			$val[] = "{$value}";
		}
		$values = '"'.implode('","', array_values($fields)).'"';

		$sql = $this->db->prepare("INSERT INTO ".$table." (`" . implode('`, `', $keys) . "`) VALUES ({$values})");
		try{
			$sql->execute();
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}

	public function select($table, $column, $value){
		$this->connect();
		$query = $this->db->prepare("SELECT * FROM ".$table." WHERE ".$column." = ?");
		$query->bindValue(1, $value);
		try{

			$query->execute();
			return $query->fetch();
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}

	public function select_all($table){
		$this->connect();
		$query = $this->db->prepare("SELECT * FROM ".$table);
		try{
			$query->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,$table);
			$query->execute();
			return $query->fetchAll(PDO::FETCH_ASSOC);
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}

	public function update($id, $table, $column, $value){
		$this->connect();
		$query = $this->db->prepare("UPDATE ".$table." SET ".$column." = ? WHERE `id`=?");
		$query->bindValue(1, $value);
		$query->bindValue(2, $id);
		try{
			$query->execute();

		}catch(PDOException $e){
			die($e->getMessage());
		}

	}

	public function delete($table, $column, $value){
		$query 	= $this->db->prepare("DELETE FROM ".$table." WHERE ".$column." = ?");
		$query->bindValue(1, $value);
		try{
			$query->execute();

		}catch(PDOException $e){
			die($e->getMessage());
		}

	}

	public function count($table, $column, $value){
		$this->connect();
		$query = $this->db->prepare("SELECT COUNT(*) FROM ".$table." WHERE ".$column."= ?");
		$query->bindValue(1, $value);
		try{
			$query->execute();
			return $query->fetchColumn();
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}
}