<?php

class User extends Database{

	private $db;
	private $table;
	private $col_user_id;
	private $col_email_address;
	private $col_password;
	private $col_first_name;
	private $col_last_name;
	private $col_middle_name;
	private $col_address;
	private $col_contact_number;
	private $col_created_at;
	private $col_updated_at;

	public function __construct($database){

		parent::__construct();

		$this->db = $database;
		$this->table = 'users';
		$this->col_id = 'id';
		$this->col_email_address = 'email_address';
		$this->col_password = 'password';
		$this->col_first_name = 'first_name';
		$this->col_last_name = 'last_name';
		$this->col_middle_name = 'middle_name';
		$this->col_address = 'address';
		$this->col_contact_number = 'contact_number';
		$this->col_created_at = 'created_at';
		$this->col_updated_at = 'updated_at';
	}

	public function login($email,$pass){
		$data = $this->select($this->table,$this->col_email_address,$email);
		try{
			$stored_password = $data['password'];
			$id = $data['id'];
			if($stored_password === sha1($pass)){
				return $id;
			}else{
				return false;
			}
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}

	//o
	public function user_exists($email){
		$rows = $this->count($this->table, $this->col_email_address, $email);
		try{
			if($rows == 1){
		  		return true;
			}else{
		  		return false;
			}
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}

	public function register(){

		$validation_setting = array(
			'email_address' => array (
				'role'    => 'required',
				'message' => 'Email is required',
			),
			'middle_name' => array(
				'role'     => 'required',
				'message'  => 'Middle name is required',
			),
		);
		$validation_result = $this->validate($validation_setting);
		if (empty($validation_result)) {
			$post_data = $_POST;
			$post_data['password'] = sha1($post_data['password']);
			$post_data['created_at'] = $this->set_date();
			unset($post_data['confpass']);
			unset($post_data['register']);
			$this->insert($this->table, $post_data);
			return true;
		} else {
			return $validation_result;
		}
	}


	private function validate($validation_setting = array()) {

		$errors = array();

		foreach ($validation_setting as $setting_key => $setting_value) {
			switch ($setting_value['role']) {
				case 'required':
					if (empty($_POST[$setting_key])) {
						$errors[$setting_key.'_error'] = $setting_value['message'];
					}
					break;
				default:
					# code...
					break;
			}
		}

		return $errors;
	}




	public function user_data($user_id){
		$query = $this->select($this->table, $this->col_id, $user_id);
		return $query;
	}

	public function get_users(){
		$query = $this->select_all($this->table);
		return $query;
	}

	public function delete_user($user_id){
		$this->delete($this->table, $this->col_id, $user_id);
	}

	public function edit_user(){
		$user_id = $_SESSION['user_id'];
		$user_data = $this->user_data($user_id);

		if($_POST['confpass'] == $_POST['password']){
			if($_POST['email_address'] != $user_data['email_address']){
				if($this->user_exists($_POST['email_address']) === true){
					$errors[] = 'Email already exists';
				}
			}
		}else{
			$errors[] = "Password does not match!";
		}
		if(empty($errors)){
			$_POST['created_at'] = $this->set_date();
			if(!empty($_POST['first_name'])){
				$this->update($user_id, $this->table, $this->col_first_name, $_POST['first_name']);
			}
			if(!empty($_POST['middle_name'])){
				$this->update($user_id, $this->table, $this->col_middle_name, $_POST['middle_name']);
			}
			if(!empty($_POST['last_name'])){
				$this->update($user_id, $this->table, $this->col_last_name, $_POST['last_name']);
			}
			if(!empty($_POST['address'])){
				$this->update($user_id, $this->table, $this->col_address, $_POST['address']);
			}
			if(!empty($_POST['contact_number'])){
				$this->update($user_id, $this->table, $this->col_contact_number, $_POST['contact_number']);
			}
			$this->update($user_id, $this->table, $this->col_updated_at, $this->set_date());
		}

	}
}
