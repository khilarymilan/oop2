<?php
	session_start();

	require 'db.class.php';
	require 'user.class.php';
	require 'post.class.php';

	$database = new Database();
	$db = $database->connect();
	$users 	= new User($db);
	$posts 	= new Post($db);