<?php
class Post extends Database{

	private $db;
	private $table;
	private $col_post_id;
	private $col_user_id;
	private $col_title;
	private $col_body;
	private $col_created_at;
	private $col_updated_at;


	public function __construct($database){
		$this->db = $database;
		$this->table = 'posts';
		$this->col_post_id = 'id';
		$this->col_user_id = 'user_id';
		$this->col_title = 'title';
		$this->col_body = 'body';
		$this->col_created_at = 'created_at';
		$this->col_updated_at = 'updated_at';
	}

	public function add_post(){

		$_POST['user_id'] = $_SESSION['user_id'];
		$_POST['created_at'] = $this->set_date();
		unset($_POST['add']);

		if($_POST['title'] == NULL || $_POST['body'] == NULL){
			return false;
		}else{
			$this->insert($this->table, $_POST);

			return true;
		}
	}

	public function get_all_posts(){
		$query = $this->select_all($this->table);
		return $query;
	}

	public function get_post($post_id){
		$query = $this->select($this->table, $this->col_post_id, $post_id);
		return $query;
	}

	public function edit_post($post_id,$column,$value){
		$this->update($post_id, $this->table, $column, $value);

	}

	public function delete_post($post_id){
		$this->delete($this->table, $this->col_post_id, $post_id);
	}


}